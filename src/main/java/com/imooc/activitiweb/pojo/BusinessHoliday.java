package com.imooc.activitiweb.pojo;

import java.time.LocalDateTime;

public class BusinessHoliday {

    private String taskId;

    private String applicant;

    private String department;

    private String leaveTime;

    private Integer leaveDays;

    private String procInstId;

    private String instTaskId;

    private String procDefId;

    private String reviewer;

    private String reviewResult;

    private String reviewComments;


    private String updater;

    private LocalDateTime updateTime;

    private String businessKey;

    private String instanceName;

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    @Override
    public String toString() {
        return "BusinessHoliday{" +
                "taskId='" + taskId + '\'' +
                ", applicant='" + applicant + '\'' +
                ", department='" + department + '\'' +
                ", leaveTime='" + leaveTime + '\'' +
                ", leaveDays=" + leaveDays +
                ", procInstId='" + procInstId + '\'' +
                ", instTaskId='" + instTaskId + '\'' +
                ", procDefId='" + procDefId + '\'' +
                ", reviewer='" + reviewer + '\'' +
                ", reviewResult='" + reviewResult + '\'' +
                ", reviewComments='" + reviewComments + '\'' +
                ", updater='" + updater + '\'' +
                ", updateTime=" + updateTime +
                ", businessKey='" + businessKey + '\'' +
                ", instanceName='" + instanceName + '\'' +
                '}';
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getProcDefId() {
        return procDefId;
    }

    public void setProcDefId(String procDefId) {
        this.procDefId = procDefId;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant == null ? null : applicant.trim();
    }



    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime == null ? null : leaveTime.trim();
    }

    public Integer getLeaveDays() {
        return leaveDays;
    }

    public void setLeaveDays(Integer leaveDays) {
        this.leaveDays = leaveDays;
    }



    public String getProcInstId() {
        return procInstId;
    }

    public void setProcInstId(String procInstId) {
        this.procInstId = procInstId == null ? null : procInstId.trim();
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer == null ? null : reviewer.trim();
    }

    public String getReviewResult() {
        return reviewResult;
    }

    public void setReviewResult(String reviewResult) {
        this.reviewResult = reviewResult;
    }

    public String getReviewComments() {
        return reviewComments;
    }

    public void setReviewComments(String reviewComments) {
        this.reviewComments = reviewComments == null ? null : reviewComments.trim();
    }

    public String getInstTaskId() {
        return instTaskId;
    }

    public void setInstTaskId(String instTaskId) {
        this.instTaskId = instTaskId;
    }


    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}